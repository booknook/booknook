#** Booknook **#

A single page web app that lets you create a personal library and organize your books. Click 'Add Book' to put a book on your shelf. Search Google Books for a title or add your own. Enjoy!

#** Installation **#

Clone the repository, `cd` into it, and run `npm install`. Once installed, run `npm start` to start the app and open `http://localhost:3000/` to view it. Run tests using `npm test`.


#** Why I Built It the Way I Did **#

The project was about an SPA. and SPAs are tricky because they don’t automatically divide their state and functionality into subunits like page-based projects do. That means that they can be more demanding when it comes to keeping UI and state in lockstep, and not painting yourself into bad corners. Because of that, I knew I needed a way to handle the extra complexity. JavaScript frameworks seemed like an obvious approach, and React was the obvious choice among them, both because your team uses it and, of course, because I’m really keen on being a part!

And so I started from there. And it was clear that React would give me a few strong advantages. First and foremost, declarative view components -- meaning I could have simple, dumb views that were easy to reason about and whose decision-making and logic could live in a matching container. I could also make (most of) those views composable by making them agnostic about their context, which was promising for productivity down the line. I also liked the fact that React was less than a framework. That meant I had the benefit of fewer implementation decisions being made on my behalf -- though, admittedly, the burden of having to pick and choose how I was going manage the other pieces of my architecture React didn’t cover.

Though my app is small at the moment, I’ve got big dreams for it ;) (or at least the project requirements mentioned a production version), and so I realized that my components might proliferate over time. I was worried about how lots of components might make my app unwieldy, particularly absent well-reasoned rules for mutating state. And so I wanted a way to make it easier to reason about the relationships between views and their backing model, and to enforce those relationships architecturally. And that led me to Redux.

Redux seemed appealing (and is proving to be) in part because it gave me some pretty good ways to corral state updates through actions, dispatches, and reducers. And to make sure that those updates only went in a single direction. That’s helped me constraint state transformations, and it seems it should make the app easier to reason about as it grows. Along the way, I added Immutable.js to make those constraints even tighter, and Router to bind view and state to URLs.

I’ve been pleased with the results so far. My post-mortem has a few of the details.



#**Post-Mortem**#

I’d worked with other JavaScript frameworks, but this was my first substantial dive into React. And I enjoyed it a lot. I was surprised at how quickly it, Redux and Router could be picked up, relative to full-fledged frameworks. I think part of that’s due to the fact that React requires choice about what you pair it with. That choice takes time to make, but the process forces you (and the community that came before you) to be more thoughtful about what’s actually going on under the hood. That seems to pay off in quicker learning -- and a better informed community all around.

I bumped up against at least two downsides. First, the community may be thoughtful about what it’s doing, but it isn’t necessarily in agreement about how it should be done. There’s lots of flux in the space (that’s a pun, I guess) and that makes for some uncertainty about whether implementation choices are here to stay or will need to be rewritten in a few months. Second, what you win in SRP and coherence (which is substantial), you lose in no longer being able to keep code as compact. React + Redux distributes feature implementation more widely than some less disciplined approaches. That makes it more painstaking to trace. But stable patterns help considerably.

Is the app production ready? Not yet. There’d be more work to be done: server-side rendering, loading states, vigorous form validation, more unit tests, etc. But I think it’s a start, and it’s been a fun one. Thanks for giving me that chance -- certainly hope for more to come!
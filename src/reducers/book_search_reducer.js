/*
 The book reducer updates bookState in response to SUCCESS-type actions passed to it.
 booksState is an immutable map.
 */

import * as actions from '../actions/actions_types';
import { Map, fromJS } from 'immutable';

const initialState = fromJS({
  query: '',
  searchResult: {},
  isLoading: false
});

export default (state = initialState,  action) => {
  switch (action.type) {
    case actions.SET_QUERY:
      return state.set('query', action.query);

    case actions.BOOK_SEARCH_SUCCESS:
      return state.set('searchResult', Map(action.searchResult));

    case actions.RESET_BOOK_SEARCH:
      return state.set('searchResult', {});

    case actions.SET_IS_LOADING:
      return state.set('isLoading', action.isLoading);

    default:
      return state;
  }
};

/*
  The book reducer updates bookState in response to SUCCESS-type actions passed to it.
  booksState is an immutable map.
 */

import * as actions from '../actions/actions_types';
import { Map, fromJS } from 'immutable';

const initialState = fromJS({
  books: [],
  book: {}
});

export default (state = initialState,  action) => {
  switch (action.type) {
    case actions.GET_BOOKS_SUCCESS:
      return state.set('books', fromJS(action.books));

    case actions.GET_BOOK_SUCCESS:
      return state.set('book', Map(action.book));

    case actions.ADD_BOOK_SUCCESS:
      return state.updateIn(['books'], arr => arr.push(Map(action.book)));

    case actions.DELETE_BOOK_SUCCESS:
      return state.setIn(['books'],
        state.getIn(['books']).filter(arr => {
          return arr.get('id') !== action.id;
        }));

    default:
      return state;
  }
};

import { fromJS, Map } from 'immutable';
import book_reducer from '../book_reducer';
import * as types from '../../actions/actions_types';

describe('books reducer', () => {
  it('should return the initial state', () => {
    expect( book_reducer(undefined, {}) )
    .toEqual( fromJS({ books: [], book: {} }) )
  });

  it('should handle GET_BOOKS_SUCCESS', () => {
    expect(
      book_reducer(undefined, {
        type: types.GET_BOOKS_SUCCESS,
        books: [{ title: 'The Fellowship of the Ring', authors: 'J.R.R. Tolkien', id: 1 }]
    }))
    .toEqual( fromJS({ books: [{ title: 'The Fellowship of the Ring', authors: 'J.R.R. Tolkien', id: 1 }], book: {} }) );

    expect(
      book_reducer( fromJS({books: [{ title: 'The Fellowship of the Ring', authors: 'J.R.R. Tolkien', id: 1 }] } ),
      {
        type: types.GET_BOOKS_SUCCESS,
        books: [
          { title: 'The Two Towers', authors: 'J.R.R. Tolkien', id: 1 },
          { title: 'The Fellowship of the Ring', authors: 'J.R.R. Tolkien', id: 2 }
        ]
      }
    ))
    .toEqual( fromJS({
      books: [
        { title: 'The Two Towers', authors: 'J.R.R. Tolkien', id: 1 },
        { title: 'The Fellowship of the Ring', authors: 'J.R.R. Tolkien', id: 2 }
      ]
    }) );
  });

  it('should handle GET_BOOK_SUCCESS', () => {
    expect(
      book_reducer(undefined, {
        type: types.GET_BOOK_SUCCESS,
        book: { title: 'Flowers for Algernon', authors: 'Daniel Keyes', id: 1 }
      }))
      .toEqual( fromJS( { books: [], book: Map({ title: 'Flowers for Algernon', authors: 'Daniel Keyes', id: 1 } ) } ) );

    expect(
      book_reducer( fromJS({ book: { title: 'The Fellowship of the Ring', authors: 'J.R.R. Tolkien', id: 1 } } ),
        {
          type: types.GET_BOOK_SUCCESS,
          book: { title: 'The Two Towers', authors: 'J.R.R. Tolkien', id: 1 }
        }
      ))
      .toEqual( fromJS({
        book: { title: 'The Two Towers', authors: 'J.R.R. Tolkien', id: 1 }
      }) );
  });

  it('should handle ADD_BOOK_SUCCESS', () => {
    expect(
      book_reducer(undefined, {
        type: types.ADD_BOOK_SUCCESS,
        book: { title: 'Harry Potter and the Chamber of Secrets', authors: 'J.K. Rowling', id: 1 }
      }))
      .toEqual( fromJS({ books: [{ title: 'Harry Potter and the Chamber of Secrets', authors: 'J.K. Rowling', id: 1 }], book: {} }) );

    expect(
      book_reducer( fromJS({ books: [{ title: 'The Fellowship of the Ring', authors: 'J.R.R. Tolkien', id: 1 }], book: {} }),
        {
          type: types.ADD_BOOK_SUCCESS,
          book: { title: 'The Two Towers', authors: 'J.R.R. Tolkien', id: 2 },
        }
      ))
      .toEqual( fromJS({
        books: [
          { title: 'The Fellowship of the Ring', authors: 'J.R.R. Tolkien', id: 1 },
          { title: 'The Two Towers', authors: 'J.R.R. Tolkien', id: 2 },
        ],
        book: {}
      }) );
  });

  it('should handle DELETE_BOOK_SUCCESS', () => {
    expect(
      book_reducer(fromJS({ books: [{ title: 'The Fellowship of the Ring', authors: 'J.R.R. Tolkien', id: 1 }] }), {
        type: types.DELETE_BOOK_SUCCESS,
        id: 1
      }))
      .toEqual( fromJS({ books: [] }) );

    expect(
      book_reducer( fromJS({ books: [
            { title: 'The Fellowship of the Ring', authors: 'J.R.R. Tolkien', id: 1 },
            { title: 'The Two Towers', authors: 'J.R.R. Tolkien', id: 2 }
          ]
        }),
        {
          type: types.DELETE_BOOK_SUCCESS,
          id: 1
        }
      ))
      .toEqual( fromJS({ books: [{ title: 'The Two Towers', authors: 'J.R.R. Tolkien', id: 2 }] }) );
  });
});
/*
 The view reducer updates the view state in response to the SET_VIEW action.
 */

import * as actions from '../actions/actions_types';

const initialState = '';

export default (state = initialState,  action) => {
  switch (action.type) {
    case actions.SET_VIEW:
      return state = action.view;

    default:
      return state;
  }
};

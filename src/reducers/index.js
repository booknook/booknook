/*
 The Redux combineReducers helper function turns an object whose values are bookReducer, sortReducer,
  viewReducer, formReducer, and bookSearchReducer into a single reducing function to create a single state.
  formReducer comes from the redux-form library.
 */

import bookReducer from './book_reducer';
import sortReducer from './sort_reducer';
import viewReducer from './view_reducer';
import bookSearchReducer from './book_search_reducer';
import { reducer as formReducer } from 'redux-form/immutable';
import { combineReducers } from 'redux';

const rootReducer = combineReducers({
  booksState: bookReducer,
  sort: sortReducer,
  view: viewReducer,
  form: formReducer,
  bookSearch: bookSearchReducer
});

export default rootReducer;

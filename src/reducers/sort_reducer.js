/*
 The sort reducer updates the sort state in response to the SET_SORT action.
 */

import * as actions from '../actions/actions_types';

const initialState = '';

export default (state = initialState,  action) => {
  switch (action.type) {
    case actions.SET_SORT:
      return state = action.sort;

    default:
      return state;
  }
};

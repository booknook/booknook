/*
 The API to make HTTP requests
 These are called in the action creators in src/actions/async_book_actions.js
 */

import request from 'superagent';
import * as constants from '../constants';

const books_resource = constants.API_URL + '/books';

/*
 Fetches books from DB
 Returns an array of books
*/

export const getBooksFromDB = () => {
  return request.get(books_resource)
    .then(response => response.body)
    .catch(error => { throw error });
};

/*
  Fetches a book from the DB
  Returns a book object
  @id : book id
 */

export const getBookFromDB = (id) => {
  return request.get(books_resource + '/' + id)
    .then(response => response.body)
    .catch(error => { throw error });
};

/*
  Adds a book to the DB
  Returns a book object
  @book : book object
 */

export const addBookToDB = (book) => {
  return request.post(books_resource, book)
    .then(response => response.body)
    .catch(error => { throw error });
};

/*
  Deletes a book from the DB
  Returns response object
  @id : book id
 */

export const deleteBookFromDB = (id) => {
  return request.delete(books_resource + '/' + id)
    .then(response => response)
    .catch(error => { throw error });
};

/*
  Uploads a photo to Cloudinary
  Returns a photo object
  @photoFile : a File object
 */

export const uploadPhotoToCloudinary = (photoFile) => {
  return request.post(constants.CLOUDINARY_API_URL)
    .field('upload_preset', constants.CLOUDINARY_UPLOAD_PRESET)
    .field('file', photoFile)
    .then(response => response.body)
    .catch(error => { throw error });
};

/*
 Search Google Books API
 */

import request from 'superagent';
import { GOOGLE_BOOKS_API_URL } from '../constants';

export const searchGoogleBooks = (query) => {
  return request.get(GOOGLE_BOOKS_API_URL + '?q=' + encodeURIComponent(query) + '&maxResults=1&printType=books')
    .then(response => response.body)
    .catch(error => { throw error });
};

/*
Sorting utilities
 */

const compareTitle = (lhs, rhs) => {
  return lhs.get('title').toLowerCase().localeCompare(rhs.get('title').toLowerCase());
};

export const sortAsc = (arr) => {
  return arr.sort((a, b) => {
    return compareTitle(a, b);
  });
};

export const sortDesc = (arr) => {
  return arr.sort((a, b) => {
    return compareTitle(b, a);
  });
};

export const sortRecent = (arr) => {
  return arr.sort((a, b) => {
    if (a.get('date') > b.get('date')) return -1;
    if (a.get('date') < b.get('date')) return 1;
    return 0;
  }
)};

/*
Utility functions used in components
 */

import React from 'react';
import { browserHistory } from 'react-router';

// Returns the book cover image for the grid and list view components

export const getBookCover = (bookImage) => {
  return bookImage
          ? <img src={ bookImage } className="book-cover" alt="Book cover" />
          : <div className="penguin-book-cover" />;
};

// Constructs a query parameter object from the current sort and view states

export const constructQueryParams = (sort, view) => {
  const queryParams = {};

  if (view) {
    queryParams.view = view;
  }
  if (sort) {
    queryParams.sort = sort;
  }
  return queryParams;
};

// Redirects to home page with the current sort and view query params

export const goToBooksPage = (sort, view, additionalParams) => {
  const queryParams = constructQueryParams(sort, view);

  browserHistory.push({
    pathname: '/',
    query: {...Object.assign({}, queryParams, additionalParams)}
  });
};

/*
  The root component.
  Uses react-redux's <Provider> component to make the Redux store available to the connect() calls in the child components
  Uses react-router to map components to the URL.
 */

import React from 'react';
import ReactDOM from 'react-dom';
import { Router, Route, browserHistory } from 'react-router';

import { Provider } from 'react-redux';
import store from './store';

import Layout from './components/layouts/Layout';
import BooksLayoutContainer from './components/containers/BooksLayoutContainer';
import PageNotFoundLayout from './components/layouts/PageNotFoundLayout';
import AddBookFormContainer from './components/containers/AddBookFormContainer';
import BookDetailsContainer from './components/containers/BookDetailsContainer';

import './styles/index.css';

ReactDOM.render(
  <Provider store={ store }>
    <Router history={ browserHistory }>
      <Route component={ Layout } >
        <Route path="/" component={ BooksLayoutContainer }>
          <Route path="/books" component={ BooksLayoutContainer } />
          <Route path="books/:id" component={ BookDetailsContainer } />
          <Route path="add_book" component={ AddBookFormContainer } />
        </Route>
        <Route path="*" component={ PageNotFoundLayout } />
      </Route>
    </Router>
  </Provider>,
  document.getElementById('root')
);

/*
 BooksNavLinkContainer renders the BooksNavLink component (the navbar containing the view and sort options)
 */

import React, { Component } from 'react';
import { connect } from 'react-redux';
import { setSort } from '../../actions/sort_actions';
import { setView } from '../../actions/view_actions';
import { constructQueryParams } from '../../utils/component_utils';
import BooksNavLink from '../views/BooksNavLink';

class BooksNavLinkContainer extends Component {

  render() {
    const { linkQueryParams, children, onSelect, handleSelect, sort, view } = this.props,
          queryParams = constructQueryParams(sort, view);

    return <BooksNavLink
              linkQueryParams={ linkQueryParams }
              children={ children }
              onSelect={ onSelect }
              currentQueryParams={ queryParams }
              handleSelect={ handleSelect }
           />;
  }
}

const mapStateToProps = (state) => ({
  view: state.view,
  sort: state.sort
});

// When a view or sort option is selected on the nav bar, either the "setSort" or "setView" action creators will be called
const mapDispatchToProps = (dispatch, ownProps) => ({
  handleSelect: () => {
    if (typeof ownProps.linkQueryParams.sort === 'string') {
      dispatch(setSort(ownProps.linkQueryParams.sort));
    }
    else {
      dispatch(setView(ownProps.linkQueryParams.view));
    }
  }
});

export default connect(mapStateToProps, mapDispatchToProps)(BooksNavLinkContainer);

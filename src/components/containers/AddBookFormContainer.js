/*
  AddBookFormContainer renders the AddBookForm component ("Add a new book" form)
  It also calls the saveNewBook action creator and contains the logic for validating the form.
 */

import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { reduxForm } from 'redux-form/immutable';
import { goToBooksPage } from '../../utils/component_utils';
import  { browserHistory } from 'react-router';
import { saveNewBook } from '../../actions/book_thunks';
import { resetBookSearch } from '../../actions/book_search_action_creators';
import AddBookForm from '../views/AddBookForm';
import PhotoUploadWidget from '../views/PhotoUploadWidget';

import '../../styles/AddBookForm.css';

class AddBookFormContainer extends Component {
  constructor(props) {
    super(props);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.closeModal = this.closeModal.bind(this);
  }

  /*
    Calls "saveNewBook" action creator after form submission
    Redirects to the book details page after book has been saved
  */

  handleSubmit(book) {
    const { saveNewBook, resetBookSearch } = this.props;

    return saveNewBook(book)
      .then(({ book }) => {
        resetBookSearch();
        browserHistory.push({
          pathname: '/books/' + book.id,
          query: {added: 'success'}
        });
      });
  }

  // Renders the photo upload widget used for drag-and-drop image uploading.

  renderDropzoneInput(field) {
    return <PhotoUploadWidget field={ field } />;
  }

  // Closes the modal window

  closeModal() {
    const { sort, view, resetBookSearch } = this.props;
    resetBookSearch();
    goToBooksPage(sort, view);
  }

  render() {
    return <AddBookForm
             renderDropzoneInput={ this.renderDropzoneInput }
             onSubmit={ this.handleSubmit }
             closeModal={ this.closeModal }
             { ...this.props }
           />;
  }
}

/*
 Form validation: Title and authors are required fields
 Returns errors object
*/

const validate = (values) => {
  const errors = {};
  if (!values.get('title')) {
    errors.title = 'Required';
  }

  if (!values.get('authors')) {
    errors.authors = 'Required';
  }

  return errors;
};


const mapStateToProps = (state) => ({
  view: state.view,
  sort: state.sort,
  initialValues: state.bookSearch.get('searchResult')
});

const mapDispatchToProps = (dispatch) => ({
  saveNewBook: bindActionCreators(saveNewBook, dispatch),
  resetBookSearch: bindActionCreators(resetBookSearch, dispatch)
});

// Connects the form container to Redux using redux-form

export default connect(mapStateToProps, mapDispatchToProps)(reduxForm({
  form: 'new_book',
  validate,
  enableReinitialize: true
})(AddBookFormContainer));

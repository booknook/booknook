/*
 BookSearchFormContainer renders the BookSearchForm component (the books search bar on the Add a Book page)
 */

import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { setQuery } from '../../actions/book_search_action_creators';
import { searchForBook } from '../../actions/book_search_thunks';

import BookSearchForm from '../views/BookSearchForm';

class BookSearchFormContainer extends Component {

  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleSubmit(e) {
    e.preventDefault();
    const {searchForBook, query} = this.props;
    if (query) {
      searchForBook(query);
    }
  }

  handleChange(e) {
    this.props.setQuery(e.target.value);
  }

  render() {
    return <BookSearchForm
              handleChange={ this.handleChange }
              handleSubmit={ this.handleSubmit }
              isLoading={ this.props.isLoading }
           />;
  }
}

const mapStateToProps = (state) => ({
  query: state.bookSearch.get('query'),
  searchResult: state.bookSearch.get('searchResult'),
  isLoading: state.bookSearch.get('isLoading')
});

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({ setQuery, searchForBook }, dispatch)
};

export default connect(mapStateToProps, mapDispatchToProps)(BookSearchFormContainer);

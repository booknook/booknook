/*
 BookDetailsContainer renders the BookDetails component
 It also calls the getBook and deleteBook action creators
 */

import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { getBook, deleteBook } from '../../actions/book_thunks';
import { goToBooksPage } from '../../utils/component_utils';
import { browserHistory } from 'react-router';
import BookDetails from '../views/BookDetails';

class BookDetailsContainer extends Component {

  constructor(props) {
    super(props);
    this.closeModal = this.closeModal.bind(this);
    this.deleteBook = this.deleteBook.bind(this);
  }

  // Fetches the book. If the book isn't found, redirects to 404 page.

  componentDidMount() {
    this.props.getBook(this.props.params.id)
      .catch(error => {
        browserHistory.push('404');
      });
  }

  // Closes the modal window and redirects to the books page

  closeModal() {
    const { sort, view } = this.props;
    goToBooksPage(sort, view);
  }

  // Calls the deleteBook action creator and redirects to the book page

  deleteBook() {
    this.props.deleteBook(this.props.book.get('id'))
      .then(response => {
        const { sort, view } = this.props;
        goToBooksPage(sort, view, { deleted: 'success'});
      });
  }

  render() {
    const isNewlyAdded = this.props.location.query.added !== undefined;

    return <BookDetails
              book={ this.props.book }
              closeModal={ this.closeModal }
              deleteBook={ this.deleteBook }
              isNewlyAdded={ isNewlyAdded }
           />
  }

}

const mapStateToProps = (state) => ({
  book: state.booksState.get('book'),
  sort: state.sort,
  view: state.view
});

const mapDispatchToProps = (dispatch) => ({
  getBook: bindActionCreators(getBook, dispatch),
  deleteBook: bindActionCreators(deleteBook, dispatch)
});

// Connects the container to the Redux store
export default connect(mapStateToProps, mapDispatchToProps)(BookDetailsContainer);

/*
 BooksListContainer renders either the GridView or the ListView components depending on the view type
  or an empty state if there are no books in the store.
 */

import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Link } from 'react-router';
import { sortBooks } from '../../selectors/book_selectors';
import { getBooks } from '../../actions/book_thunks';

import GridView from '../views/GridView';
import ListView from '../views/ListView';

class BooksListContainer extends Component {

  // Fetches the List of books

  componentDidMount() {
    this.props.getBooks();
  }

  render() {
    const { books, view } = this.props;

    if (books.size === 0) {
      return <p style={{marginLeft: '10px'}}>You have no books. <Link to="/add_book" style={{color: '#5cb85c'}}>Add a book</Link> to get started.</p>;
    }

    return view === 'list'
      ? <ListView books={ books } />
      : <GridView books={ books } />;
  }
}

const mapStateToProps = (state) => ({
  books: sortBooks(state),
  view: state.view
});

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({ getBooks }, dispatch)
};

export default connect(mapStateToProps, mapDispatchToProps)(BooksListContainer);

/*
 BooksLayoutContainer renders the BooksLayout component
 */

import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { setView } from '../../actions/view_actions';
import { setSort } from '../../actions/sort_actions';
import BooksLayout from '../layouts/BooksLayout';

class BooksLayoutContainer extends Component {

  constructor(props) {
    super(props);

    // Grab the sort and view query parameters out of the URL and save them to state.
    // This only happens once, when the component is rendered.
    const queryParams = this.props.location.query;
    if (queryParams.sort) {
      this.props.setSort(queryParams.sort);
    }
    if (queryParams.view) {
      this.props.setView(queryParams.view);
    }
  }

  render() {
    return <BooksLayout { ...this.props } />;
  }
}

const mapDispatchToProps = (dispatch) => ({
  setSort: bindActionCreators(setSort, dispatch),
  setView: bindActionCreators(setView, dispatch)
});

export default connect(null, mapDispatchToProps)(BooksLayoutContainer);

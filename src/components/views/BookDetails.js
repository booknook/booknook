/*
  BookDetails contains the Book Details modal window.
 */

import React, { PropTypes } from 'react';
import { getBookCover } from '../../utils/component_utils';
import { Modal, Button, Alert } from 'react-bootstrap';

import '../../styles/BookDetails.css';

const BookDetails = ({ book, closeModal, deleteBook, isNewlyAdded }) => {
  const bookDescription = book.get('description'),
        bookCategories = book.get('categories'),
        successMessage = isNewlyAdded && <Alert bsStyle="success" className="book-added-success">Book added.</Alert>;

  return (
    <Modal show={ true } onHide={ closeModal } dialogClassName="BookDetails">
      <Modal.Header closeButton>
        { successMessage }
        <Modal.Title>{ book.get('title') }</Modal.Title>
        <h5>by { book.get('authors') }</h5>
      </Modal.Header>
      <Modal.Body>
        { getBookCover(book.get('image')) }
        <div className="book-info-wrapper" >
          <h4>Description:</h4>
          <p>{ bookDescription ? bookDescription : <span className="not-provided">N/A</span> }</p>
          <h4>Category:</h4>
          <p>{ bookCategories ? bookCategories : <span className="not-provided">N/A</span> }</p>
        </div>
      </Modal.Body>
      <Modal.Footer>
        <Button onClick={ deleteBook }>Delete</Button>
      </Modal.Footer>
    </Modal>
  );
};

BookDetails.propTypes = {
  book: PropTypes.object.isRequired,
  closeModal: PropTypes.func.isRequired,
  deleteBook: PropTypes.func.isRequired,
  isNewlyAdded: PropTypes.bool
};

export default BookDetails;

/*
Renders a thumbnail on the books page grid view
 */

import React, { PropTypes } from 'react';
import { Link } from 'react-router';
import * as utils from '../../utils/component_utils';
import { Col } from 'react-bootstrap';
import Immutable  from 'immutable';

const Book = ({ book }) => (
  <Col className="column" xs={12} sm={4} key={ book.get('id') }>
    <div className="book-wrapper">
      <Link to={ {pathname: `/books/${book.get('id')}`} }>
        { utils.getBookCover(book.get('image')) }
        <div className="book-info-wrapper">
          <h4 className="book-title">{ book.get('title') }</h4>
          <p className="book-authors">{ book.get('authors') }</p>
        </div>
      </Link>
    </div>
  </Col>
);

Book.propTypes = {
  book: PropTypes.instanceOf(Immutable.Map)
};

export default Book;
/*
 BooksNavBar contains the navbar with the sort and view options.
 */

import React from 'react';
import { Navbar, Nav, NavDropdown } from 'react-bootstrap';
import { Link } from 'react-router';
import BooksNavLinkContainer from '../containers/BooksNavLinkContainer';

import '../../styles/BooksNavBar.css';

export default () => (
  <Navbar className="BooksNavBar" default staticTop>
    <Link to="add_book" className="add-book"><span className="glyphicon glyphicon-plus" />Add Book</Link>
    <Nav pullRight>
      <NavDropdown id="view" title="View">
        <BooksNavLinkContainer linkQueryParams={ {view: 'grid'} } children={ [<span className="glyphicon glyphicon-th"/>, "Grid"] } />
        <BooksNavLinkContainer linkQueryParams={ {view: 'list'} } children={ [<span className="glyphicon glyphicon-th-list"/>, "List"] } />
      </NavDropdown>
      <NavDropdown id="sort" title="Sort" className="sort-options" >
        <BooksNavLinkContainer linkQueryParams={ {sort: 'asc'} } children={ ["Ascending by Title"] } />
        <BooksNavLinkContainer linkQueryParams={ {sort: 'desc'} }  children={ ["Descending by Title"] } />
        <BooksNavLinkContainer linkQueryParams={ {sort: 'recent'} } children={ ["Recently Added"] } />
      </NavDropdown>
    </Nav>
  </Navbar>
);

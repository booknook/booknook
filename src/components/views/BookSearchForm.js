/*
BookSearchForm renders the books search form on the Add a Book page
 */

import React from 'react';
import { FormGroup, FormControl, Button } from 'react-bootstrap';

import '../../styles/BookSearchForm.css';

export default ({ handleChange, handleSubmit, isLoading }) => (
  <div className="BookSearchForm">
    <form className="form-inline" onSubmit={ handleSubmit }>
      <FormGroup>
        <FormControl type="text" placeholder="Search for a book title..." onChange={ handleChange } />
      </FormGroup>
      <Button type="submit" disabled={ isLoading }>
        Search
      </Button>
    </form>
  </div>
);

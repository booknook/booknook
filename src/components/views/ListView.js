/*
 ListView contains the books list laid out in list view.
 */

import React, { PropTypes } from 'react';
import { Link } from 'react-router';
import { Media } from 'react-bootstrap';
import * as utils from '../../utils/component_utils';
import Immutable  from 'immutable';

import '../../styles/book_list.css';
import '../../styles/ListView.css';

const ListView = (props) => {
  const bookItems = props.books.map((book, index, bookArray) => {

    return (
      <Media className="book-wrapper" key={book.get('id')}>
        <Link to={ {pathname: `/books/${book.get('id')}`} }>
          <Media.Left>
            { utils.getBookCover(book.get('image')) }
          </Media.Left>
          <Media.Body>
            <div className="book-info-wrapper">
              <h4 className="book-title">{ book.get('title') }</h4>
              <p className="book-authors">{ book.get('authors') }</p>
            </div>
          </Media.Body>
        </Link>
      </Media>
    );
  });

  return (
    <div className="ListView book-list">
      { bookItems }
    </div>
  );
};

/*
 @books: an immutable list of books
 */

ListView.propTypes = {
  books: PropTypes.instanceOf(Immutable.List)
};

export default ListView;

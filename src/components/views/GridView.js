/*
 GridView contains the books list laid out in a three-column grid view
 */

import React, { PropTypes } from 'react';
import Immutable  from 'immutable';
import BooksRow from './BooksRow';

import '../../styles/book_list.css';
import '../../styles/GridView.css';

const GridView = ({ books }) => {

  const bookItems = books.reduce((triplets, book, index) => {
    if(index % 3 === 0) {
      triplets.push([]);
    }
    triplets[triplets.length - 1].push(book);
    return triplets;
  }, []).map((triplets, index) => (
        <BooksRow key={ index } bookTriplets={ triplets } />
  ));

  return (
    <div className="GridView book-list">
      { bookItems }
    </div>
  );
};

/*
  @books: an immutable list of books
 */

GridView.propTypes = {
  books: PropTypes.instanceOf(Immutable.List)
};

export default GridView;

/*
BooksRow renders a row of books on the books page grid view
 */

import React, { PropTypes } from 'react';
import { Row } from 'react-bootstrap';
import Book from './Book';

export default ({ bookTriplets }) => (
  <Row>
    {
      bookTriplets.map((book, index) => (
        <Book key={ index } book={ book }/>
      ))
    }
  </Row>
);

React.propTypes = {
  bookTriplets: PropTypes.array.isRequired
};
/*
 NavBar contains the main app navbar.
 */

import React from 'react';
import { Navbar } from 'react-bootstrap';

import '../../styles/NavBar.css';

export default () => (
  <Navbar staticTop className="NavBar">
    <Navbar.Header>
      <h1>Bo<span className="ok">ok</span>nook</h1>
    </Navbar.Header>
  </Navbar>
);

/*
 BooksNavLink contains a link item for the book navbar (e.g. "Ascending by Title").
 */

import React, { PropTypes } from 'react';
import { MenuItem } from 'react-bootstrap';
import { LinkContainer } from 'react-router-bootstrap';

const BooksNavLink = ({ linkQueryParams, children, handleSelect, onSelect, currentQueryParams }) => {
  const linkChildren = children.map((childElement, index) =>
    <span key={ index }>{ childElement }</span>
  );

  return (
    <LinkContainer to={{pathname: '/', query: Object.assign({}, currentQueryParams, linkQueryParams) }}>
      <MenuItem onClick={ handleSelect } onSelect={ onSelect }>
        { linkChildren }
      </MenuItem>
    </LinkContainer>
  );
};

/*
  @linkQueryParams: the sort or view type object for the link (e.g. { view: 'grid' })
  @children: The text or HTML markup to be displayed in link (must be passed as an array to handle HTML markup)
  @currentQueryParams: the query params currently in the URL (e.g. { view: 'list', sort: 'asc' })
 */

BooksNavLink.propTypes = {
  linkQueryParams: PropTypes.object.isRequired,
  children: PropTypes.array.isRequired,
  currentQueryParams: PropTypes.object.isRequired
};

export default BooksNavLink;

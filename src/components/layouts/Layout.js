/*
 The Layout component contains the layout for the bookshop app.
*/

import React from 'react';
import NavBar from '../views/NavBar'
import '../../styles/Layout.css';

export default (props) => {
  return (
    <div className="Layout">
      <header>
        <NavBar />
      </header>
      { props.children }
    </div>
  );
};

/*
 The BooksLayout component contains the layout for the books page.
 It renders BookListContainer as well as BookDetailsContainer and AddBookFormContainer in { props.children }
 */

import React from 'react';
import { Grid, Row, Col, Alert } from 'react-bootstrap';
import BooksNavBar from '../views/BooksNavBar';
import BooksListContainer from '../containers/BooksListContainer';

import '../../styles/BooksLayout.css';

export default (props) => {
  const successMessage = props.location.query.deleted &&
      <Alert bsStyle="success" className="book-added-success">Book was deleted.</Alert>;

  return (
    <div className="BooksLayout">
      <BooksNavBar />
      <Grid>
        <Row>
          <Col>
            { successMessage }
            <BooksListContainer />
          </Col>
        </Row>
      </Grid>
      { props.children }
    </div>
  );
};

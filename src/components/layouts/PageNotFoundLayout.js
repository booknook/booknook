/*
  PageNotFoundLayout is rendered on unrecognized routes
 */

import React from 'react';
import { Grid } from 'react-bootstrap';

export default (props) => {
  return (
    <Grid>
      <h1>Page not found.</h1>
    </Grid>
  );
};

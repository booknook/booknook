// App constants

export const API_URL = 'http://localhost:3001';
export const CLOUDINARY_API_URL = 'https://api.cloudinary.com/v1_1/dup3qruxm/upload';
export const CLOUDINARY_UPLOAD_PRESET = 'cymtefdt';
export const GOOGLE_BOOKS_API_URL = 'https://www.googleapis.com/books/v1/volumes';

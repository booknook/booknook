/*
  Redux's "createStore" creates a Redux store that holds the complete state of the app.
  Using "applyMiddleware" to enable thunk, which makes async store dispatches.
 */

import { createStore, compose, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import rootReducer from './reducers';

/* REDUX DEVTOOLS -- In case you need them. */
const finalCreateStore = compose(
  applyMiddleware(thunk),
  window.devToolsExtension ? window.devToolsExtension() : f => f
)(createStore);

const store = finalCreateStore(rootReducer);
export default store;

/*
  The sortBooks selector uses the reselect library to
  sort the list of books in booksState.books only when the sort state updates.
  Returns a list of books sorted ascending or descending by title or sorted by recently added.
 */

import { createSelector } from 'reselect'
import * as utils from '../utils/utilities';

const getSortFilter = (state) => state.sort;
const getBooks = (state) => state.booksState.get('books');

export const sortBooks = createSelector(
  [ getSortFilter, getBooks ],
  (sortFilter, books) => {
    switch (sortFilter) {
      case 'desc':
        return utils.sortDesc(books);

      case 'recent':
        return utils.sortRecent(books);

      default:
        return utils.sortAsc(books);
    }
  }
);
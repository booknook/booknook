/*
 Redux action for updating the view state
 Send to the store using store.dispatch()
 */

import * as actions from './actions_types';

/*
 Called to set the books list view (grid or list)
 Returns the view type
 @view : view type
 */

export const setView = (view) => ({
  type: actions.SET_VIEW,
  view
});

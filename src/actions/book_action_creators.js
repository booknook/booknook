/*
  Redux action creators for all updates pertaining to the books state.
  Send them to the store using store.dispatch()
 */

import * as actions from './actions_types';

// Called at the start of a books array request

export const getBooksRequest = () => ({
  type: actions.GET_BOOKS_REQUEST
});

/*
  Called after fetching an array of books from the DB
  Returns a books array
  @books : an array of books
 */

export const getBooksSuccess = books => ({
  type: actions.GET_BOOKS_SUCCESS,
  books
});

// Called at the start of a request to add a book

export const addBookRequest = () => ({
  type: actions.ADD_BOOK_REQUEST
});

/*
  Called after adding a book to the DB.
  Returns a book object
  @book : a book object
*/

export const addBookSuccess = book => ({
  type: actions.ADD_BOOK_SUCCESS,
  book
});

// Called at the start of a request to delete a book

export const deleteBookRequest = () => ({
  type: actions.DELETE_BOOK_REQUEST
});

/*
  Called after a book is deleted from the DB.
  Returns the deleted book's id
  @id : book id
 */

export const deleteBookSuccess = id => ({
  type: actions.DELETE_BOOK_SUCCESS,
  id
});

// Called at the start of a request to fetch a book from the DB

export const getBookRequest = () => ({
  type: actions.GET_BOOK_REQUEST
});

/*
  Called after a book is fetched from the DB
  Returns a book object
  @book : book object
 */

export const getBookSuccess = book => ({
  type: actions.GET_BOOK_SUCCESS,
  book
});

// Called at the start of a request to upload a photo

export const uploadPhotoRequest = () => ({
  type: actions.UPLOAD_PHOTO_REQUEST
});

// Called after photo has been successfully uploaded

export const uploadPhotoSuccess = () => ({
  type: actions.UPLOAD_PHOTO_SUCCESS
});

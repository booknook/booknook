import * as bookSearchAPI from '../api/book_search_api';
import * as actions from './book_search_action_creators';

/*
 Calls the book search API to search for a book
 Dispatches bookSearchSuccess on successful completion
 @query : a search term
 */

export const searchForBook = (query) => {
  return (dispatch) => {
    dispatch(actions.setIsLoading(true));
    dispatch(actions.bookSearchRequest());
    return bookSearchAPI.searchGoogleBooks(query)
      .then(response => {
        const searchResult = response.items[0].volumeInfo,
              book = {};

        book.title = searchResult.title;
        if (searchResult.authors) {
          book.authors = searchResult.authors.join(', ');
        }
        if (searchResult.categories) {
          book.categories = searchResult.categories.join(', ');
        }
        book.description = searchResult.description;
        if (searchResult.imageLinks) {
          book.image = searchResult.imageLinks.thumbnail;
        }
        dispatch(actions.setIsLoading(false));
        return dispatch(actions.bookSearchSuccess(book));
      })
      .catch(error => { throw error })
  };
};

/*
 Action types that represent the different actions that can be taken on the books, sort, view, and book search states.
 */

// Book action types

export const GET_BOOKS_REQUEST = 'GET_BOOKS_REQUEST';
export const GET_BOOKS_SUCCESS = 'GET_BOOKS_SUCCESS';
export const GET_BOOK_REQUEST = 'GET_BOOK_REQUEST';
export const GET_BOOK_SUCCESS = 'GET_BOOK_SUCCESS';
export const ADD_BOOK_REQUEST = 'ADD_BOOK_REQUEST';
export const ADD_BOOK_SUCCESS = 'ADD_BOOK_SUCCESS';
export const DELETE_BOOK_REQUEST = 'DELETE_BOOK_REQUEST';
export const DELETE_BOOK_SUCCESS = 'DELETE_BOOK_SUCCESS';

export const UPLOAD_PHOTO_REQUEST = 'UPLOAD_PHOTO_REQUEST';
export const UPLOAD_PHOTO_SUCCESS = 'UPLOAD_PHOTO_SUCCESS';

// Sort action type

export const SET_SORT = 'SET_SORT';

// View action type

export const SET_VIEW = 'SET_VIEW';

// Book search types

export const SET_QUERY= 'SET_QUERY';
export const BOOK_SEARCH_REQUEST = 'BOOK_SEARCH_REQUEST';
export const BOOK_SEARCH_SUCCESS = 'BOOK_SEARCH_SUCCESS';
export const RESET_BOOK_SEARCH = 'RESET_BOOK_SEARCH';
export const SET_IS_LOADING = 'SET_IS_LOADING';

/*
These action creators use the redux-thunk middleware to make async dispatches.
They are called directly from React containers to update the DB and books state.
 */

import * as bookAPI from '../api/book_api';
import * as actions from './book_action_creators';

/*
 Adds a book to the DB and the books state.
 This method is not available as part of the public API, but is called in the "saveNewBook" method below.
 Returns an immutable book Map
 @book: an immutable book Map
 */

const addBook = (book) => {
  return (dispatch) => {
    dispatch(actions.addBookRequest());
    return bookAPI.addBookToDB(book)
      .then(response => dispatch(actions.addBookSuccess(response)))
      .then(({ book }) => {
        return dispatch(actions.getBookSuccess(book))
      })
      .catch(error => {
        throw error;
      });
  };
};

/*
Returns an immutable List of books.
 */

export const getBooks = () => {
  return (dispatch) => {
    dispatch(actions.getBooksRequest());
    return bookAPI.getBooksFromDB().then(
      books => dispatch(actions.getBooksSuccess(books)),
      error => {
        throw error;
      }
    );
  };
};

/*
Returns an immutable Map of a single book.
@id : The book id
 */

export const getBook = (id) => {
  return (dispatch) => {
    dispatch(actions.getBookRequest());
    return bookAPI.getBookFromDB(id).then(
      book => dispatch(actions.getBookSuccess(book)),
      error => {
        throw error;
      }
    );
  };
};

/*
Deletes a book.
@id : The book id
 */

export const deleteBook = (id) => {
  return (dispatch) => {
    dispatch(actions.deleteBookRequest());
    return bookAPI.deleteBookFromDB(id).then(
      book => dispatch(actions.deleteBookSuccess(id)),
      error => {
        throw error;
      }
    );
  };
};

/*
  Saves a new book.
  If an image file is passed in the "book" map, saveNewBook will call the book API to upload the photo.
  Calls "addBook" to add the book to the DB and the books state.
  Returns an immutable book Map.
  @book : an immutable book Map
 */

export const saveNewBook = (book) => {
  return (dispatch) => {
    const photoFile = book.get('image'),
          updatedBook = book.set('date', Date.now());

    if (photoFile) {
      dispatch(actions.uploadPhotoRequest());
      return bookAPI.uploadPhotoToCloudinary(photoFile)
        .then(response => {
          dispatch(actions.uploadPhotoSuccess());
          const bookWithPhoto = updatedBook.set('image', response.secure_url);
          return dispatch(addBook(bookWithPhoto));
        })
        .catch(error => {
          throw error;
        });
    }

    return dispatch(addBook(updatedBook));
  };
};


/*
 Redux action creators for all updates pertaining to the Google books search state.
 Send them to the store using store.dispatch()
 */

import * as actions from './actions_types';

// Called to set the search query

export const setQuery = (query) => ({
  type: actions.SET_QUERY,
  query
});

// Called at the start of a book search request

export const bookSearchRequest = () => ({
  type: actions.BOOK_SEARCH_REQUEST
});

/*
 Called after the book search completed successfully
 @searchResult : a book object
 */
export const bookSearchSuccess = (searchResult) => ({
  type: actions.BOOK_SEARCH_SUCCESS,
  searchResult
});

// Called to reset the book search state

export const resetBookSearch = () => ({
  type: actions.RESET_BOOK_SEARCH
});

// Sets isLoading

export const setIsLoading = (isLoading) => ({
  type: actions.SET_IS_LOADING,
  isLoading
});

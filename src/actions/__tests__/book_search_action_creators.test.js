import * as actions from '../book_search_action_creators';
import * as types from '../actions_types';

describe('book actions', () => {

  it('should create an action to set the search query', () => {
    const expectedAction = {
      type: types.SET_QUERY
    };
    expect(actions.setQuery()).toEqual(expectedAction);
  });

  it('should create an action to start a book search request', () => {
    const expectedAction = {
      type: types.BOOK_SEARCH_REQUEST
    };
    expect(actions.bookSearchRequest()).toEqual(expectedAction);
  });

  it('should create an action to start a book search request', () => {
    const expectedAction = {
      type: types.BOOK_SEARCH_SUCCESS
    };
    expect(actions.bookSearchSuccess()).toEqual(expectedAction);
  });

  it('should create an action to reset the book search state', () => {
    const expectedAction = {
      type: types.RESET_BOOK_SEARCH
    };
    expect(actions.resetBookSearch()).toEqual(expectedAction);
  });
});

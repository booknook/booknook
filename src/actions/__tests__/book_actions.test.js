import * as actions from '../book_action_creators';
import * as types from '../actions_types';

describe('book actions', () => {

  it('should create an action after requesting a list of books', () => {
    const expectedAction = {
      type: types.GET_BOOKS_REQUEST
    };
    expect(actions.getBooksRequest()).toEqual(expectedAction);
  });

  it('should create an action after successfully fetching a list of books', () => {
    const books = [{ title: 'The Fellowship of the Ring', authors: 'J.R.R. Tolkien', id: 1 }];
    const expectedAction = {
      type: types.GET_BOOKS_SUCCESS,
      books
    };
    expect(actions.getBooksSuccess(books)).toEqual(expectedAction);
  });

  it('should create an action after requesting to add a book', () => {
    const expectedAction = {
      type: types.ADD_BOOK_REQUEST
    };
    expect(actions.addBookRequest()).toEqual(expectedAction);
  });

  it('should create an action after successfully adding a book', () => {
    const book = { title: 'The Fellowship of the Ring', authors: 'J.R.R. Tolkien', id: 1 };
    const expectedAction = {
      type: types.ADD_BOOK_SUCCESS,
      book
    };
    expect(actions.addBookSuccess(book)).toEqual(expectedAction);
  });

  it('should create an action after requesting to delete a book', () => {
    const expectedAction = {
      type: types.DELETE_BOOK_REQUEST
    };
    expect(actions.deleteBookRequest()).toEqual(expectedAction);
  });

  it('should create an action after successfully deleting a book', () => {
    const expectedAction = {
      type: types.DELETE_BOOK_SUCCESS,
      id: 1
    };
    expect(actions.deleteBookSuccess(1)).toEqual(expectedAction);
  });

  it('should create an action after requesting a single book', () => {
    const expectedAction = {
      type: types.GET_BOOK_REQUEST
    };
    expect(actions.getBookRequest()).toEqual(expectedAction);
  });

  it('should create an action after successfully getting a single book', () => {
    const book = { title: 'The Fellowship of the Ring', authors: 'J.R.R. Tolkien', id: 1 };
    const expectedAction = {
      type: types.GET_BOOK_SUCCESS,
      book: book
    };
    expect(actions.getBookSuccess(book)).toEqual(expectedAction);
  });

  it('should create an action after requesting to upload a photo', () => {
    const expectedAction = {
      type: types.UPLOAD_PHOTO_REQUEST
    };
    expect(actions.uploadPhotoRequest()).toEqual(expectedAction);
  });

  it('should create an action after successfully uploading a photo', () => {
    const expectedAction = {
      type: types.UPLOAD_PHOTO_SUCCESS
    };
    expect(actions.uploadPhotoSuccess()).toEqual(expectedAction);
  });
});

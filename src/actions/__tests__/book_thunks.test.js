import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import nock from 'nock';
import * as actions from '../book_thunks';
import * as types from '../actions_types';
import { API_URL, CLOUDINARY_API_URL } from '../../constants';
import { Map } from 'immutable';

const middleware = [ thunk ];
const mockStore = configureMockStore(middleware);

describe('book async actions', () => {

  afterEach(() => {
    nock.cleanAll()
  });

  it('creates GET_BOOKS_SUCCESS after fetching books', () => {
      nock(API_URL)
        .get('/books')
        .reply(200, [{ title: 'The Fellowship of the Ring', authors: 'J.R.R. Tolkien', id: 1 }]);

      const expectedActions = [
        { type: types.GET_BOOKS_REQUEST },
        { type: types.GET_BOOKS_SUCCESS, books: [{ title: 'The Fellowship of the Ring', authors: 'J.R.R. Tolkien', id: 1 }]}
      ];

      const store = mockStore({ books: [] });

      return store.dispatch(actions.getBooks())
        .then(() => {
          expect(store.getActions()).toEqual(expectedActions);
        })
  });


  it('creates GET_BOOK_SUCCESS after fetching a single book', () => {
    nock(API_URL)
      .get('/books/1')
      .reply(200, { title: 'The Fellowship of the Ring', authors: 'J.R.R. Tolkien', id: 1 });

    const expectedActions = [
      { type: types.GET_BOOK_REQUEST },
      { type: types.GET_BOOK_SUCCESS, book: { title: 'The Fellowship of the Ring', authors: 'J.R.R. Tolkien', id: 1 }}
    ];

    const store = mockStore({ book: {} });

    return store.dispatch(actions.getBook(1))
      .then(() => {
        expect(store.getActions()).toEqual(expectedActions);
      })
  });

  it('creates DELETE_BOOK_SUCCESS after deleting a book', () => {
    nock(API_URL)
      .delete('/books/1')
      .reply(200, 1);

    const expectedActions = [
      { type: types.DELETE_BOOK_REQUEST },
      { type: types.DELETE_BOOK_SUCCESS, id: 1 }
    ];

    const store = mockStore({ books: [] });

    return store.dispatch(actions.deleteBook(1))
      .then(() => {
        expect(store.getActions()).toEqual(expectedActions);
      })
  });

  it('creates ADD_BOOK_SUCCESS after adding a book without a photo', () => {
    nock(API_URL)
      .post('/books', { title: 'The Fellowship of the Ring', authors: 'J.R.R. Tolkien'})
      .reply(200, { title: 'The Fellowship of the Ring', authors: 'J.R.R. Tolkien', id: 1});

    const expectedActions = [
      { type: types.ADD_BOOK_REQUEST },
      { type: types.ADD_BOOK_SUCCESS, book: { title: 'The Fellowship of the Ring', authors: 'J.R.R. Tolkien', id: 1} },
      { type: types.GET_BOOK_SUCCESS, book: { title: 'The Fellowship of the Ring', authors: 'J.R.R. Tolkien', id: 1} }
    ];

    const store = mockStore({ book: {} });

    return store.dispatch(actions.saveNewBook( Map({ title: 'The Fellowship of the Ring', authors: 'J.R.R. Tolkien'})) )
      .then(() => {
        expect(store.getActions()).toEqual(expectedActions);
      })
  });

  it('creates ADD_BOOK_SUCCESS after adding a book with a photo', () => {
    nock(CLOUDINARY_API_URL)
      .post('')
      .reply(200, {secure_url: 'https://res.cloudinary.com/abc/image/upload/ring.jpg' }
      );

    nock(API_URL)
      .post('/books', { title: 'The Fellowship of the Ring', authors: 'J.R.R. Tolkien',
          image: 'https://res.cloudinary.com/abc/image/upload/ring.jpg'
      })
      .reply(200, { title: 'The Fellowship of the Ring', authors: 'J.R.R. Tolkien', id: 1,
          image: 'https://res.cloudinary.com/abc/image/upload/ring.jpg'
      });

    const expectedActions = [
      { type: types.UPLOAD_PHOTO_REQUEST },
      { type: types.UPLOAD_PHOTO_SUCCESS },
      { type: types.ADD_BOOK_REQUEST },
      { type: types.ADD_BOOK_SUCCESS, book: { title: 'The Fellowship of the Ring', authors: 'J.R.R. Tolkien', id: 1,
          image: 'https://res.cloudinary.com/abc/image/upload/ring.jpg' } },
      { type: types.GET_BOOK_SUCCESS, book: { title: 'The Fellowship of the Ring', authors: 'J.R.R. Tolkien', id: 1,
        image: 'https://res.cloudinary.com/abc/image/upload/ring.jpg' } }
    ];

    const store = mockStore({ book: {} });

    return store.dispatch(actions.saveNewBook( Map({ title: 'The Fellowship of the Ring', authors: 'J.R.R. Tolkien',
          image: 'https://res.cloudinary.com/abc/image/upload/ring.jpg'})) )
      .then(() => {
        expect(store.getActions()).toEqual(expectedActions);
      })
  });
});

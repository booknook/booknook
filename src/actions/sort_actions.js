/*
 Redux action for updating the sort state
 Send to the store using store.dispatch()
 */

import * as actions from './actions_types';

/*
  Called to set the book sorting order (asc, desc, or recent)
  Returns the sort type
  @sort : sort type
*/

export const setSort = (sort) => ({
  type: actions.SET_SORT,
  sort
});
